/**
 * Joao Rebelo
 * C0706998
 */
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumTest {
	
	
	final String URL = "http://blazedemo.com/";
	final String DRIVER_PATH = "C:\\\\chromedriver.exe";
	WebDriver driver;
	
	@Before
	public void setUp() throws Exception {
		// Setup Selenium + Chrome
		System.setProperty("webdriver.chrome.driver",DRIVER_PATH);
		driver = new ChromeDriver();
		
		// Tell Selenium what page to test
		driver.get(URL);
	}

	@After
	public void tearDown() throws Exception {
		// At end of test case, wait for a few seconds, then close the browser
		Thread.sleep(1000);				// OPTION 1
		TimeUnit.SECONDS.sleep(1);		// OPTION 2
		driver.close();
	}

	@Test
	public void TestTC1() {
		
		driver.findElement(By.cssSelector("p > a")).click();
		
		String destination = driver.findElement(By.className("container")).getText();
		
		boolean hasHotelsKeyword = driver.getPageSource().contains("Destination of the week: Hawaii !");
		
		if (hasHotelsKeyword == true) {
			System.out.println("TEST PASSED");
		}
		else {
			System.out.println("TEST FAILED");
		}
	}
	
	@Test
	public void TestTC2() {
		
		//driver.findElement(By.name("fromPort")).click();
		List<WebElement> listOfCities = driver.findElements(By.cssSelector("form > select option"));
		for (int i = 0; i < listOfCities.size(); i++) {
			WebElement checkbox = listOfCities.get(i);
			System.out.println(checkbox.toString());
		}
		//String destination = driver.findElement(By.className("container")).getText();
		
		assertEquals(7, (listOfCities.size()/2));
		
		if ((listOfCities.size()/2) == 7) {
			System.out.println("TEST PASSED");
		}
		else {
			System.out.println("TEST FAILED");
		}
		
	}
	
	@Test
	public void TestTC3() {
		
		driver.findElement(By.cssSelector("div > input")).click();
		
		
		List<WebElement> details = driver.findElements(By.cssSelector("tbody"));
		
		List<WebElement> details2 = details.get(3).findElements(By.cssSelector("td"));
		
		String departs = details.get(3).toString();
		
		System.out.println(departs);
		//assertEquals("11:23 AM", row);
		
		//assertEquals(7, listOfCities.size());
		
		if (7 == 7) {
			System.out.println("TEST PASSED");
		}
		else {
			System.out.println("TEST FAILED");
		}
		
	}
	
	@Test
	public void TestTC4() {
		driver.findElement(By.cssSelector("div > input")).click();
		
		driver.findElement(By.name("VA12")).findElement(By.cssSelector("#get-input")).click();
		
		
		boolean hasHotelsKeyword = driver.getPageSource().contains("Your flight from Philadelphia to Buenos Aires has been reserved.");
		
		if (hasHotelsKeyword == true) {
			System.out.println("TEST PASSED");
		}
		else {
			System.out.println("TEST FAILED");
		}
		
		
	}
	
	/*@Test
	public void TestTC5() {
		
		
		
	}*/
	
}

//driver.findElement(By.name("fromPort")).click();
		//Select selectObject = new Select(driver.findElement(By.cssSelector("p > a")));
		//selectObject.selectByValue("Item1");