/**
 * Joao Rebelo
 * C0706998
 */
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SnakeTest {

	private Snake peter;
	private Snake takis;
	
	@Before
	public void setUp() throws Exception {
		peter = new Snake("Peter S", 10, "coffee");
		takis = new Snake("Takis Z", 80, "vegetables");
	}
	
	@Test
	public void isHealthy() {
		assertEquals(false, peter.isHealthy());
		assertEquals(true, takis.isHealthy());
	}
	
	@Test
	public void fitsInCage() {
		assertEquals(false, peter.fitsInCage(8));
		assertEquals(true, peter.fitsInCage(10));
		assertEquals(true, peter.fitsInCage(20));
	} 

}